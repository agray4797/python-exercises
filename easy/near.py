def near(strng1, strng2):
    for i in range(0, len(strng1)):
        compstrng = ''.join([strng1[n] for n in range (0, len(strng1)) if n != i])
        if compstrng == strng2:
            return True
            break
        else:
            pass
    else:
        return False


u_strng1 = str(input("What is your first string? "))
u_strng2 = str(input("What is your second string? "))
if len(u_strng1) > len(u_strng2):
    print(near(u_strng1, u_strng2))
elif len(u_strng1) == len(u_strng2):
    print("Strings must be of different lengths!")
else:
    print(near(u_strng2, u_strng1))
